//------------------------------------------------------------------------------
// Copyright (c) 2019, Michel Rizzo.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: libFrame
// Displays, in a frame, a set of lines after buffering them
//------------------------------------------------------------------------------
#ifndef FRAME_H
#define FRAME_H
//------------------------------------------------------------------------------
// MACROS DEFINITIONS
//------------------------------------------------------------------------------
#define NOFRAME			0x001
#define TEXTFRAME		0x002
#define SINGLEFRAME		0x004
#define DOUBLEFRAME		0x008
#define BLOCKFRAME		0x010
#define BOLDFRAME		0x080
#define CENTERFRAME		0x100
#define LEFTFRAME		0x200
#define RIGHTFRAME		0x400
//------------------------------------------------------------------------------
// STRUCTURE
//------------------------------------------------------------------------------
struct sFrame;
//------------------------------------------------------------------------------
// EXTERNAL FUNCTIONS
//------------------------------------------------------------------------------
extern struct sFrame 	*frame_ini();
extern void				frame_add(struct sFrame *, const char *, ...);
extern void				frame_close(struct sFrame *, FILE *, unsigned short);
extern void 			frame_abort(struct sFrame *);
extern int 				frame_testWidth(struct sFrame *, unsigned short);

#endif	// FRAME_H
//------------------------------------------------------------------------------
