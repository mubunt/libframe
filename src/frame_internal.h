//------------------------------------------------------------------------------
// Copyright (c) 2019, Michel Rizzo.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: libFrame
// Displays, in a frame, a set of lines after buffering them
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdarg.h>
#include <errno.h>
#include <fcntl.h>
#ifdef LINUX
#include <wchar.h>
#include <locale.h>
#include <sys/ioctl.h>
#else
#include <windows.h>
#endif
//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "frame.h"
//------------------------------------------------------------------------------
// MACROS DEFINITIONS
//------------------------------------------------------------------------------
#define TERM						"TERM"
#define MSYSCON						"MSYSCON"
#define ERROR_PREFIX				"FRAME ERROR"

#define MAX(a,b)					(((a)>(b))?(a):(b))

#define MAXBUFFERLENGTH				2048

// XTERM-LIKE GRAPHIC CHARSET
#ifdef LINUX
#define WBEGCOLOR 					L"\033["
#define WENDCOLOR					L'm'
#define WXBOLD						L"\033[1m"
#define WXUNDERSCORE				L"\033[4m"
#define WXBLINK						L"\033[5m"
#define WXREVERSE					L"\033[6m"
#define WXRESET						L"\033[0m"
#define WXGRAPHMODEON				L"\033(0"
#define WXGRAPHMODEOFF				L"\033(B"
#endif
#define BEGCOLOR 					"\033["
#define ENDCOLOR					'm'
#define XBOLD						"\033[1m"
#define XUNDERSCORE					"\033[4m"
#define XBLINK						"\033[5m"
#define XREVERSE					"\033[6m"
#define XRESET						"\033[0m"
#define XGRAPHMODEON				"\033(0"
#define XGRAPHMODEOFF				"\033(B"
#define XSRBC						106	// Single Right Bottom Corner
#define XSRUC						107	// Single Right Up Corner
#define XSLUC						108	// Single Left Up Corner
#define XSLBC						109	// Single Left Bottom Corner
#define	XSH							113	// Single Horizontal
#define XSV							120	// Single Vertical
// WINDOWS CMD GRAPHIC CHARSET
// https://en.wikipedia.org/wiki/Code_page_437
#define SV							179	// Single Vertical
#define DV							186	// Double Vertical
#define DRUC						187	// Double Right Up Corner
#define DRBC						188	// Double Right Bottom Corner
#define SRUC						191	// Single Right Up Corner
#define SLBC						192	// Single Left Bottom Corner
#define	SH							196	// Single Horizontal
#define DLBC						200	// Double Left Bottom Corner
#define DLUC						201	// Double Left Up Corner
#define DH							205	// Double Horizontal
#define SRBC						217	// Single Right Bottom Corner
#define SLUC						218	// Single Left Up Corner

#define CHECK_BIT(var, pos)			(((var)>>(pos)) & 1)
#define NOFRAME_REQUIRED(var)		CHECK_BIT(var, 0)
#define TEXTFRAME_REQUIRED(var)		CHECK_BIT(var, 1)
#define SINGLEFRAME_REQUIRED(var)	CHECK_BIT(var, 2)
#define DOUBLEFRAME_REQUIRED(var)	CHECK_BIT(var, 3)
#define BLOCKFRAME_REQUIRED(var)	CHECK_BIT(var, 4)
#define BOLD_REQUIRED(var)			CHECK_BIT(var, 7)
#define CENTER_REQUIRED(var)		CHECK_BIT(var, 8)
#define LEFT_REQUIRED(var)			CHECK_BIT(var, 9)
#define RIGHT_REQUIRED(var)			CHECK_BIT(var, 10)
//------------------------------------------------------------------------------
// ENUMERATION
//------------------------------------------------------------------------------
typedef enum {
	XTERM,
	XTERMWIN,
	CMDWIN
} eTerminalType;
//------------------------------------------------------------------------------
// STRUCTURE
//------------------------------------------------------------------------------
struct sFrame {
	char			__tmpname[128];		// tempory file name for frame data
	FILE 			*__fdbuff;			// Pointer of type file for frame data
	size_t			__width;			// Max line length of frame data (visible data)
	size_t			__currentWidth;		// For internal needs (multi-lines)
	char			*__spaces;			// buffer for frame alignment
	eTerminalType	__terminalType;		// Terminal type for printing
};
//------------------------------------------------------------------------------
// INTERNAL FUNCTIONS
//------------------------------------------------------------------------------
void 			__frame_basicDisplayError(char *);
void 			__frame_displayError(char *, eTerminalType);
void 			__frame_header(struct sFrame *, FILE *, unsigned short);
#ifdef LINUX
void 			__frame_line(struct sFrame *, FILE *, size_t, unsigned short, wchar_t *);
#else
void 			__frame_line(struct sFrame *, FILE *, size_t, unsigned short, char *);
#endif
void 			__frame_trailer(struct sFrame *, FILE *, unsigned short);
void 			__frame_error(eTerminalType, const char *, ...);
eTerminalType 	__frame_getTerminalType();
size_t 			__frame_winwidth();
unsigned short 	__frame_CheckMode(unsigned short);
//-----------	-------------------------------------------------------------------
