//------------------------------------------------------------------------------
// Copyright (c) 2019, Michel Rizzo.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: libFrame
// Displays, in a frame, a set of lines after buffering them
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "frame_internal.h"
//------------------------------------------------------------------------------
struct sFrame *frame_ini() {
	struct sFrame *sF;
	int fd;
	char template[] = "./.frameXXXXXX";

	// Allocate in memory, a 'frame' descriptor block
	if (NULL == (sF = (struct sFrame *) malloc (sizeof(struct sFrame)))) {
		__frame_error(XTERM, "%s", strerror(errno));
	}
	// Find the type of terminal (xterm, xterm-windows, cmd-windows)
	sF->__terminalType = __frame_getTerminalType();
	// Get a temporary file name and open it as pointer of file (and not file secriptor)
	strcpy(sF->__tmpname, template);
	if (-1 == (fd = mkstemp(sF->__tmpname))) {
		free(sF);
		__frame_error(XTERM, "%s", strerror(errno));
		// Never reached... Just to remove warning message from "cppcheck"
		exit(-1);
	}
	sF->__fdbuff = fdopen(fd, "w+");
	// Intialize the rest of the frame' descriptor block
	sF->__currentWidth = sF->__width = 0;
	sF->__spaces = malloc(1);
	*sF->__spaces = '\0';
	//sF->__spaces = NULL;
	return(sF);
}
//------------------------------------------------------------------------------
