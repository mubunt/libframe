//------------------------------------------------------------------------------
// Copyright (c) 2019, Michel Rizzo.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: libFrame
// Displays, in a frame, a set of lines after buffering them
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "frame_internal.h"
//------------------------------------------------------------------------------
void frame_add( struct sFrame *sF, const char *format, ... ) {
	va_list al;
	char buff[MAXBUFFERLENGTH];
	size_t n, i, k, invisibleChars;

	va_start(al, format);
	vsprintf(buff, format, al);
	va_end(al);

	n = fwrite(buff, 1, strlen(buff), sF-> __fdbuff);
	k = invisibleChars = 0;

#ifdef LINUX
	setlocale(LC_ALL, "");
	wchar_t wbuff[MAXBUFFERLENGTH];
	mbstowcs(wbuff, buff, n * sizeof(wchar_t));
	n = wcslen(wbuff);
	wchar_t *pt;
	pt = wbuff;
	for (i = 0; i < n; i++) {
		if (wbuff[i] == '\n') {
			k = i;
			wbuff[i] = '\0';
			sF->__width = MAX(sF->__width, (sF->__currentWidth + wcslen(pt) - invisibleChars));
			invisibleChars = sF->__currentWidth = 0;
			pt = wbuff + i + 1;
		} else {
			if ((sF->__terminalType == XTERM) || (sF->__terminalType == XTERMWIN)) {
				wchar_t *ptmp;
				size_t j;
				ptmp = wbuff + i;
				if ((wcsncmp(ptmp, WXBOLD, wcslen(WXBOLD)) == 0) ||
				        (wcsncmp(ptmp, WXUNDERSCORE, wcslen(WXUNDERSCORE)) == 0) ||
				        (wcsncmp(ptmp, WXBLINK, wcslen(WXBLINK)) == 0) ||
				        (wcsncmp(ptmp, WXREVERSE, wcslen(WXREVERSE)) == 0) ||
				        (wcsncmp(ptmp, WXRESET, wcslen(WXRESET)) == 0)) {
					invisibleChars += wcslen(WXBOLD);
				} else {
					if ((wcsncmp(ptmp, WXGRAPHMODEON, wcslen(WXGRAPHMODEON)) == 0) ||
					        (wcsncmp(ptmp, WXGRAPHMODEOFF, wcslen(WXGRAPHMODEOFF)) == 0)) {
						invisibleChars += wcslen(WXGRAPHMODEON);
					} else {
						if (wcsncmp(ptmp, WBEGCOLOR, wcslen(WBEGCOLOR)) == 0)  {
							ptmp = ptmp + wcslen(WBEGCOLOR);
							invisibleChars = invisibleChars + wcslen(WBEGCOLOR);
							while (*ptmp != WENDCOLOR && *ptmp != '\0' ) {
								++invisibleChars;
								++ptmp;
							}
							if (*ptmp != '\0') {
								++invisibleChars;
								++ptmp;
							}
						}
					}
				}
			}
		}
	}
#else
	char *pt;
	pt = buff;
	for (i = 0; i < n; i++) {
		if (buff[i] == '\n') {
			k = i;
			buff[i] = '\0';
			sF->__width = MAX(sF->__width, (sF->__currentWidth + strlen(pt) - invisibleChars));
			invisibleChars = sF->__currentWidth = 0;
			pt = buff + i + 1;
		} else {
			if ((sF->__terminalType == XTERM) || (sF->__terminalType == XTERMWIN)) {
				char *ptmp;
				size_t j;
				ptmp = buff + i;
				if ((strncmp(ptmp, XBOLD, strlen(XBOLD)) == 0) ||
				        (strncmp(ptmp, XUNDERSCORE, strlen(XUNDERSCORE)) == 0) ||
				        (strncmp(ptmp, XBLINK, strlen(XBLINK)) == 0) ||
				        (strncmp(ptmp, XREVERSE, strlen(XREVERSE)) == 0) ||
				        (strncmp(ptmp, XRESET, strlen(XRESET)) == 0)) {
					invisibleChars += strlen(XBOLD);
				} else {
					if ((strncmp(ptmp, XGRAPHMODEON, strlen(XGRAPHMODEON)) == 0) ||
					        (strncmp(ptmp, XGRAPHMODEOFF, strlen(XGRAPHMODEOFF)) == 0)) {
						invisibleChars += strlen(XGRAPHMODEON);
					} else {
						if (strncmp(ptmp, BEGCOLOR, strlen(BEGCOLOR)) == 0)  {
							ptmp = ptmp + strlen(BEGCOLOR);
							invisibleChars = invisibleChars + strlen(BEGCOLOR);
							while (*ptmp != ENDCOLOR && *ptmp != '\0' ) {
								++invisibleChars;
								++ptmp;
							}
							if (*ptmp != '\0') {
								++invisibleChars;
								++ptmp;
							}
						}
					}
				}
			}
		}
	}
#endif
	if (k != n - 1) {
		sF->__currentWidth = sF->__currentWidth + n - k;
	}
	sF->__width = MAX(sF->__width, sF->__currentWidth);
}
//------------------------------------------------------------------------------
