//------------------------------------------------------------------------------
// Copyright (c) 2019, Michel Rizzo.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: libFrame
// Displays, in a frame, a set of lines after buffering them
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "frame_internal.h"
//------------------------------------------------------------------------------
int frame_testWidth( struct sFrame *sF, unsigned short mode ) {
#ifdef LINUX
	mode = __frame_CheckMode(mode);
	size_t n = __frame_winwidth();
	if (CHECK_BIT(mode, 0)) 	// NOFRAME
		return(sF->__width <= n ? 1 : 0);
	else
		return((sF->__width + 4) <= n ? 1 : 0);
#else
	return(1);
#endif
}

//------------------------------------------------------------------------------
