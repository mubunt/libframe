//------------------------------------------------------------------------------
// Copyright (c) 2019, Michel Rizzo.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: libFrame
// Displays, in a frame, a set of lines after buffering them
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "frame_internal.h"
//------------------------------------------------------------------------------
void frame_close( struct sFrame *sF, FILE *fdout, unsigned short mode ) {
	size_t n;

	mode = __frame_CheckMode(mode);
	char *line;
	line = malloc(MAXBUFFERLENGTH);

	size_t widthterm = __frame_winwidth();
	if (widthterm != 0) {
		if (! CHECK_BIT(mode, 0)) widthterm -= 4;
		if (widthterm > sF->__width && (CENTER_REQUIRED(mode) || RIGHT_REQUIRED(mode))) {
			n = widthterm - sF->__width;
			if (CENTER_REQUIRED(mode)) n = n / 2;
			if (sF->__spaces != NULL) free(sF->__spaces);
			sF->__spaces = malloc(n + 1);
			for (size_t i = 0; i < n; i++) sF->__spaces[i] = ' ';
			sF->__spaces[n] = '\0';
		} else {
			sF->__spaces = malloc(1);
			*sF->__spaces = '\0';
		}
	}

	rewind(sF->__fdbuff);
	__frame_header(sF, fdout, mode);

#ifdef LINUX
	setlocale(LC_ALL, "");
	wchar_t *wline;
	wline = (wchar_t *)malloc(MAXBUFFERLENGTH * sizeof(wchar_t));
	while ((fgets(line, MAXBUFFERLENGTH, sF->__fdbuff)) != NULL) {
		mbstowcs(wline, line, MAXBUFFERLENGTH);
		n = wcslen(wline);
		if (wline[n - 1] == '\n') {
			wline[n - 1] = '\0';
			--n;
		}
		if ((sF->__terminalType == XTERM) || (sF->__terminalType == XTERMWIN)) {
			size_t i, k, invisibleChars;
			wchar_t *ptmp;
			invisibleChars = 0;
			for (i = 0; i < n; i++) {
				ptmp = wline + i;
				if ((wcsncmp(ptmp, WXBOLD, wcslen(WXBOLD)) == 0) ||
				        (wcsncmp(ptmp, WXUNDERSCORE, wcslen(WXUNDERSCORE)) == 0) ||
				        (wcsncmp(ptmp, WXBLINK, wcslen(WXBLINK)) == 0) ||
				        (wcsncmp(ptmp, WXREVERSE, wcslen(WXREVERSE)) == 0) ||
				        (wcsncmp(ptmp, WXRESET, wcslen(WXRESET)) == 0)) {
					invisibleChars += wcslen(WXBOLD);
				} else {
					if ((wcsncmp(ptmp, WXGRAPHMODEON, wcslen(WXGRAPHMODEON)) == 0) ||
					        (wcsncmp(ptmp, WXGRAPHMODEOFF, wcslen(WXGRAPHMODEOFF)) == 0)) {
						invisibleChars += wcslen(WXGRAPHMODEON);
					} else {
						if (wcsncmp(ptmp, WBEGCOLOR, wcslen(WBEGCOLOR)) == 0)  {
							ptmp = ptmp + wcslen(WBEGCOLOR);
							invisibleChars = invisibleChars + wcslen(WBEGCOLOR);
							while (*ptmp != WENDCOLOR && *ptmp != '\0' ) {
								++invisibleChars;
								++ptmp;
							}
							if (*ptmp != '\0') {
								++invisibleChars;
								++ptmp;
							}
						}
					}
				}
			}
			n = n - invisibleChars;
		}
		__frame_line(sF, fdout, n, mode, wline);
	}
	free(wline);
#else
	while ((fgets(line, MAXBUFFERLENGTH, sF->__fdbuff)) != NULL) {
		n = strlen(line);
		if (line[n - 1] == '\n') {
			line[n - 1] = '\0';
			--n;
		}
		if ((sF->__terminalType == XTERM) || (sF->__terminalType == XTERMWIN)) {
			size_t i, k, invisibleChars;
			char *ptmp;
			invisibleChars = 0;
			for (i = 0; i < n; i++) {
				ptmp = line + i;
				if ((strncmp(ptmp, XBOLD, strlen(XBOLD)) == 0) ||
				        (strncmp(ptmp, XUNDERSCORE, strlen(XUNDERSCORE)) == 0) ||
				        (strncmp(ptmp, XBLINK, strlen(XBLINK)) == 0) ||
				        (strncmp(ptmp, XREVERSE, strlen(XREVERSE)) == 0) ||
				        (strncmp(ptmp, XRESET, strlen(XRESET)) == 0)) {
					invisibleChars += strlen(XBOLD);
				} else {
					if ((strncmp(ptmp, XGRAPHMODEON, strlen(XGRAPHMODEON)) == 0) ||
					        (strncmp(ptmp, XGRAPHMODEOFF, strlen(XGRAPHMODEOFF)) == 0)) {
						invisibleChars += strlen(XGRAPHMODEON);
					} else {
						if (strncmp(ptmp, BEGCOLOR, strlen(BEGCOLOR)) == 0)  {
							ptmp = ptmp + strlen(BEGCOLOR);
							invisibleChars = invisibleChars + strlen(BEGCOLOR);
							while (*ptmp != ENDCOLOR && *ptmp != '\0' ) {
								++invisibleChars;
								++ptmp;
							}
							if (*ptmp != '\0') {
								++invisibleChars;
								++ptmp;
							}
						}
					}
				}
			}
			n = n - invisibleChars;
		}
		__frame_line(sF, fdout, n, mode, line);
	}
#endif
	__frame_trailer(sF, fdout, mode);
	free(line);
	(void)fclose(sF->__fdbuff);
	(void)unlink(sF->__tmpname);
	if (sF->__spaces != NULL) free(sF->__spaces);
	free(sF);
	sF = NULL;
}
//------------------------------------------------------------------------------
