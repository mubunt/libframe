//------------------------------------------------------------------------------
// Copyright (c) 2019, Michel Rizzo.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: libFrame
// Displays, in a frame, a set of lines after buffering them
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "frame_internal.h"
//------------------------------------------------------------------------------
// MACROS
//------------------------------------------------------------------------------
#define SPACE_COMPLEMENT(out, from, to)		for (i = from; i < to; i++) fprintf(out, " ");
//------------------------------------------------------------------------------
// INTERNAL FUNCTIONS
//------------------------------------------------------------------------------
void __frame_basicDisplayError(char *label) {
	fprintf(stderr, "\n%s: %s\n\n", ERROR_PREFIX, label);
}
//------------------------------------------------------------------------------
unsigned short __frame_CheckMode(unsigned short mode) {
	unsigned short m = mode & 0x7f;
	if (m != NOFRAME && m != TEXTFRAME && m != SINGLEFRAME && m != DOUBLEFRAME && m != BLOCKFRAME) {
		mode &= 0xf80;
		mode |= NOFRAME;
	}
	m = mode & 0x700;
	if (m != CENTERFRAME && m != LEFTFRAME && m != RIGHTFRAME) {
		mode &= 0xff;
		mode |= LEFTFRAME;
	}
	return mode;
}
//------------------------------------------------------------------------------
void __frame_displayError(char *label, eTerminalType format) {
	unsigned int i;

	// Default to do graphical output when connected to a TTY but don't output the
	// escape sequences when connected to a file

	switch (format) {
	case CMDWIN:		// Windows command line and Windows/Bash
		if (isatty(2)) {
			fprintf(stderr, "\n%c", SLUC);
			fprintf(stderr, " %s ", ERROR_PREFIX);
			for (i = 0; i < (strlen(label) - strlen(ERROR_PREFIX)); i++) fprintf(stderr, "%c", SH);
			fprintf(stderr, "%c\n", SRUC);
			fprintf(stderr, " %c %s %c\n", SV, label, SV);
			fprintf(stderr, " %c", SLBC);
			for (i = 0; i < (strlen(label) + 2); i++) fprintf(stderr, "%c", SH);
			fprintf(stderr, "%c\n\n", SRBC);
		} else
			__frame_basicDisplayError(label);
		break;
	case XTERM:			// Linux (XTERM)
		if (isatty(2)) {
			fprintf(stderr, "\n%s%c%s", XGRAPHMODEON, XSLUC, XGRAPHMODEOFF);
			fprintf(stderr, " %s%s%s ", XBOLD, ERROR_PREFIX, XRESET);
			fprintf(stderr, "%s", XGRAPHMODEON);
			for (i = 0; i < (strlen(label) - strlen(ERROR_PREFIX)); i++) fprintf(stderr, "%c", XSH);
			fprintf(stderr, "%c%s\n", XSRUC, XGRAPHMODEOFF);
			fprintf(stderr, " %s%c%s %s %s%c%s\n", XGRAPHMODEON, XSV, XGRAPHMODEOFF, label, XGRAPHMODEON, XSV, XGRAPHMODEOFF);
			fprintf(stderr, " %s%c", XGRAPHMODEON, XSLBC);
			for (i = 0; i < (strlen(label) + 2); i++) fprintf(stderr, "%c", XSH);
			fprintf(stderr, "%c%s\n", XSRBC, XGRAPHMODEOFF);
		} else
			__frame_basicDisplayError(label);
		break;
	case XTERMWIN:		// and MinGW shell
		// Be careful: isatty does not run on MinGW!!!
		fprintf(stderr, "\n%s%c%s", XGRAPHMODEON, XSLUC, XGRAPHMODEOFF);
		fprintf(stderr, " %s%s%s ", XBOLD, ERROR_PREFIX, XRESET);
		fprintf(stderr, "%s", XGRAPHMODEON);
		for (i = 0; i < (strlen(label) - strlen(ERROR_PREFIX)); i++) fprintf(stderr, "%c", XSH);
		fprintf(stderr, "%c%s\n", XSRUC, XGRAPHMODEOFF);
		fprintf(stderr, " %s%c%s %s %s%c%s\n", XGRAPHMODEON, XSV, XGRAPHMODEOFF, label, XGRAPHMODEON, XSV, XGRAPHMODEOFF);
		fprintf(stderr, " %s%c", XGRAPHMODEON, XSLBC);
		for (i = 0; i < (strlen(label) + 2); i++) fprintf(stderr, "%c", XSH);
		fprintf(stderr, "%c%s\n", XSRBC, XGRAPHMODEOFF);
		break;
	}
}
//------------------------------------------------------------------------------
size_t __frame_winwidth() {
#ifdef LINUX
	struct winsize ws;
	ioctl(1, TIOCGWINSZ, &ws);
	return(ws.ws_col);
#else
	return 0;
#endif
}
//------------------------------------------------------------------------------
void __frame_header(struct sFrame *sF, FILE *fdout, unsigned short mode) {
	size_t i;

	fprintf(fdout, "\n");
	if (NOFRAME_REQUIRED(mode)) {
		// nothing to do
	}
	if (TEXTFRAME_REQUIRED(mode)) {
		if (sF->__terminalType == CMDWIN) {
			fprintf(fdout, "%s+", sF->__spaces);
			for (i = 0; i < (sF->__width + 2); i++) fprintf(fdout, "-");
			fprintf(fdout, "+\n");
		} else {
			if (BOLD_REQUIRED(mode)) fprintf(fdout, "%s", XBOLD);
			fprintf(fdout, "%s+", sF->__spaces);
			for (i = 0; i < (sF->__width + 2); i++) fprintf(fdout, "-");
			fprintf(fdout, "+\n");
			if (BOLD_REQUIRED(mode)) fprintf(fdout, "%s", XRESET);
		}
	}
	if (SINGLEFRAME_REQUIRED(mode)) {
		if (sF->__terminalType == CMDWIN) {
			fprintf(fdout, "%s%c", sF->__spaces, SLUC);
			for (i = 0; i < (sF->__width + 2); i++) fprintf(fdout, "%c", SH);
			fprintf(fdout, "%c\n", SRUC);
		} else {
			if (BOLD_REQUIRED(mode)) fprintf(fdout, "%s", XBOLD);
			fprintf(fdout, "%s%s%c", sF->__spaces, XGRAPHMODEON, XSLUC);
			for (i = 0; i < (sF->__width + 2); i++) fprintf(fdout, "%c", XSH);
			fprintf(fdout, "%c%s\n", XSRUC, XGRAPHMODEOFF);
			if (BOLD_REQUIRED(mode)) fprintf(fdout, "%s", XRESET);
		}
	}
	if (DOUBLEFRAME_REQUIRED(mode)) {
		if (sF->__terminalType == CMDWIN) {
			fprintf(fdout, "%s%c", sF->__spaces, DLUC);
			for (i = 0; i < (sF->__width + 2); i++) fprintf(fdout, "%c", DH);
			fprintf(fdout, "%c\n", DRUC);
		} else {
			if (BOLD_REQUIRED(mode)) fprintf(fdout, "%s", XBOLD);
			fprintf(fdout, "%s╔", sF->__spaces);
			for (i = 0; i < (sF->__width + 2); i++) fprintf(fdout, "═");
			fprintf(fdout, "╗\n");
			if (BOLD_REQUIRED(mode)) fprintf(fdout, "%s", XRESET);
		}
	}
	if (BLOCKFRAME_REQUIRED(mode)) {
		if (sF->__terminalType == CMDWIN) {
			fprintf(fdout, "%s%c", sF->__spaces, DLUC);
			for (i = 0; i < (sF->__width + 2); i++) fprintf(fdout, "%c", DH);
			fprintf(fdout, "%c\n", DRUC);
		} else {
			if (BOLD_REQUIRED(mode)) fprintf(fdout, "%s", XBOLD);
			fprintf(fdout, "%s▛", sF->__spaces);
			for (i = 0; i < (sF->__width + 2); i++) fprintf(fdout, "▀");
			fprintf(fdout, "▜\n");
			if (BOLD_REQUIRED(mode)) fprintf(fdout, "%s", XRESET);
		}
	}
}
//------------------------------------------------------------------------------
#ifdef LINUX
void __frame_line(struct sFrame *sF, FILE *fdout, size_t width, unsigned short mode, wchar_t *buff) {
	size_t i;

	if (NOFRAME_REQUIRED(mode)) {
		if (BOLD_REQUIRED(mode)) fprintf(fdout, "%s", XBOLD);
		fprintf(fdout, "%s%ls", sF->__spaces, buff);
		if (BOLD_REQUIRED(mode)) fprintf(fdout, "%s", XRESET);
	}
	if (TEXTFRAME_REQUIRED(mode)) {
		if (BOLD_REQUIRED(mode)) fprintf(fdout, "%s", XBOLD);
		fprintf(fdout, "%s%c %ls", sF->__spaces, '|', buff);
		SPACE_COMPLEMENT(fdout, width, sF->__width);
		fprintf(fdout, " %c", '|');
		if (BOLD_REQUIRED(mode)) fprintf(fdout, "%s", XRESET);
	}
	if (SINGLEFRAME_REQUIRED(mode)) {
		if (BOLD_REQUIRED(mode)) fprintf(fdout, "%s", XBOLD);
		fprintf(fdout, "%s%s%c%s %ls", sF->__spaces, XGRAPHMODEON, XSV, XGRAPHMODEOFF, buff);
		SPACE_COMPLEMENT(fdout, width, sF->__width);
		fprintf(fdout, " %s%c%s", XGRAPHMODEON, XSV, XGRAPHMODEOFF);
		if (BOLD_REQUIRED(mode)) fprintf(fdout, "%s", XRESET);
	}
	if (DOUBLEFRAME_REQUIRED(mode)) {
		if (BOLD_REQUIRED(mode)) fprintf(fdout, "%s", XBOLD);
		fprintf(fdout, "%s║ %ls", sF->__spaces, buff);
		SPACE_COMPLEMENT(fdout, width, sF->__width);
		fprintf(fdout, " ║");
		if (BOLD_REQUIRED(mode)) fprintf(fdout, "%s", XRESET);
	}
	if (BLOCKFRAME_REQUIRED(mode)) {
		if (BOLD_REQUIRED(mode)) fprintf(fdout, "%s", XBOLD);
		fprintf(fdout, "%s▋ %ls", sF->__spaces, buff);
		SPACE_COMPLEMENT(fdout, width, sF->__width);
		fprintf(fdout, " ▐");
		if (BOLD_REQUIRED(mode)) fprintf(fdout, "%s", XRESET);
	}
	fprintf(fdout, "\n");
}
#else
void __frame_line(struct sFrame *sF, FILE *fdout, size_t width, unsigned short mode, char *buff) {
	size_t i;

	if (NOFRAME_REQUIRED(mode)) {
		fprintf(fdout, "%s%s", sF->__spaces, buff);
	}
	if (TEXTFRAME_REQUIRED(mode)) {
		fprintf(fdout, "%s%c %s", sF->__spaces, '|', buff);
		SPACE_COMPLEMENT(fdout, width, sF->__width);
		fprintf(fdout, " %c", '|');
	}
	if (SINGLEFRAME_REQUIRED(mode)) {
		if (sF->__terminalType == CMDWIN) {
			fprintf(fdout, "%s%c %s", sF->__spaces, SV, buff);
			SPACE_COMPLEMENT(fdout, width, sF->__width);
			fprintf(fdout, " %c", SV);
		} else {
			fprintf(fdout, "%s%s%c%s %s", sF->__spaces, XGRAPHMODEON, XSV, XGRAPHMODEOFF, buff);
			SPACE_COMPLEMENT(fdout, width, sF->__width);
			fprintf(fdout, " %s%c%s", XGRAPHMODEON, XSV, XGRAPHMODEOFF);
		}
	}
	if (DOUBLEFRAME_REQUIRED(mode)) {
		if (sF->__terminalType == CMDWIN) {
			fprintf(fdout, "%s%c %s", sF->__spaces, DV, buff);
			SPACE_COMPLEMENT(fdout, width, sF->__width);
			fprintf(fdout, " %c", DV);
		} else {
			fprintf(fdout, "%s║ %s", sF->__spaces, buff);
			SPACE_COMPLEMENT(fdout, width, sF->__width);
			fprintf(fdout, " ║");
		}
	}
	if (BLOCKFRAME_REQUIRED(mode)) {
		if (sF->__terminalType == CMDWIN) {
			fprintf(fdout, "%s%c %s", sF->__spaces, DV, buff);
			SPACE_COMPLEMENT(fdout, width, sF->__width);
			fprintf(fdout, " %c", DV);
		} else {
			fprintf(fdout, "%s▋ %s", sF->__spaces, buff);
			SPACE_COMPLEMENT(fdout, width, sF->__width);
			fprintf(fdout, " ▐");
		}
	}
	fprintf(fdout, "\n");
}
#endif
//------------------------------------------------------------------------------
void __frame_trailer(struct sFrame *sF, FILE *fdout, unsigned short mode) {
	size_t i;

	if (NOFRAME_REQUIRED(mode)) {
		// nothing to do
	}
	if (TEXTFRAME_REQUIRED(mode)) {
		if (sF->__terminalType == CMDWIN) {
			fprintf(fdout, "%s+", sF->__spaces);
			for (i = 0; i < (sF->__width + 2); i++) fprintf(fdout, "-");
			fprintf(fdout, "+\n");
		} else {
			if (BOLD_REQUIRED(mode)) fprintf(fdout, "%s", XBOLD);
			fprintf(fdout, "%s+", sF->__spaces);
			for (i = 0; i < (sF->__width + 2); i++) fprintf(fdout, "-");
			fprintf(fdout, "+\n");
			if (BOLD_REQUIRED(mode)) fprintf(fdout, "%s", XRESET);
		}
	}
	if (SINGLEFRAME_REQUIRED(mode)) {
		if (sF->__terminalType == CMDWIN) {
			fprintf(fdout, "%s%c", sF->__spaces, SLBC);
			for (i = 0; i < (sF->__width + 2); i++) fprintf(fdout, "%c", SH);
			fprintf(fdout, "%c\n", SRBC);
		} else {
			if (BOLD_REQUIRED(mode)) fprintf(fdout, "%s", XBOLD);
			fprintf(fdout, "%s%s%c", sF->__spaces, XGRAPHMODEON, XSLBC);
			for (i = 0; i < (sF->__width + 2); i++) fprintf(fdout, "%c", XSH);
			fprintf(fdout, "%c%s\n", XSRBC, XGRAPHMODEOFF);
			if (BOLD_REQUIRED(mode)) fprintf(fdout, "%s", XRESET);
		}
	}
	if (DOUBLEFRAME_REQUIRED(mode)) {
		if (sF->__terminalType == CMDWIN) {
			fprintf(fdout, "%s%c", sF->__spaces, DLBC);
			for (i = 0; i < (sF->__width + 2); i++) fprintf(fdout, "%c", DH);
			fprintf(fdout, "%c\n", DRBC);
		} else {
			if (BOLD_REQUIRED(mode)) fprintf(fdout, "%s", XBOLD);
			fprintf(fdout, "%s╚", sF->__spaces);
			for (i = 0; i < (sF->__width + 2); i++) fprintf(fdout, "═");
			fprintf(fdout, "╝\n");
			if (BOLD_REQUIRED(mode)) fprintf(fdout, "%s", XRESET);
		}
	}
	if (BLOCKFRAME_REQUIRED(mode)) {
		if (sF->__terminalType == CMDWIN) {
			fprintf(fdout, "%s%c", sF->__spaces, DLBC);
			for (i = 0; i < (sF->__width + 2); i++) fprintf(fdout, "%c", DH);
			fprintf(fdout, "%c\n", DRBC);
		} else {
			if (BOLD_REQUIRED(mode)) fprintf(fdout, "%s", XBOLD);
			fprintf(fdout, "%s▙", sF->__spaces);
			for (i = 0; i < (sF->__width + 2); i++) fprintf(fdout, "▄");
			fprintf(fdout, "▟\n");
			if (BOLD_REQUIRED(mode)) fprintf(fdout, "%s", XRESET);
		}
	}
	fprintf(fdout, "\n");
}
//-----------------------------------------------------------------------------
eTerminalType __frame_getTerminalType() {
	char *pt;

	if (NULL == getenv(TERM))					// Windows Command Line
		return(CMDWIN);
	if ((pt = getenv(MSYSCON)) == NULL)			// Linux shell
		return(XTERM);
	if (strcmp(pt, "sh.exe") == 0)				// Windows/Bash shell
		return(CMDWIN);
	return(XTERMWIN);
}
//------------------------------------------------------------------------------
void __frame_error(eTerminalType term, const char *label, ...) {
	va_list al;
	char buff[512];

	va_start(al, label);
	vsprintf(buff, label, al);
	va_end(al);
	__frame_displayError(buff, term);
	fflush(stderr);
	exit(-1);
}
//------------------------------------------------------------------------------
