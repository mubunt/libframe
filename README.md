 # *libFrame*, Displays, in a frame, a set of lines after buffering them.

This library provides routines to display a set of lines with a frame. This frame can be textual or graphical.
A graphic frame can consist of a single line (*single frame)*, a double line (*double frame*) or a block (*block frame*).

**Examples:**

*No Frame:*
![libFrame](README_images/1.png "No Frame")

*Text Frame:*
![libFrame](README_images/2.png "Text Frame")

*Single Frame:*
![libFrame](README_images/3.png "Single Frame")

*Double Frame:*
![libFrame](README_images/4.png "Double Frame")

*Block Frame (xterm-like only):*
![libFrame](README_images/5.png "Block Frame (xterm-like only)")

On linux, text may have accented characters (wide characters).

## LICENSE
**libFrame** is covered by the GNU General Public License (GPL) version 3 and above.

## API DEFINITION
### frame_ini
*Synopsys:*
```C
struct sFrame *frame_ini();
```
*Description:*
This function prepares the memorization of the lines to be displayed.

*Return value:*
A pointer to a structure of type *sFrame* is returned. This structure will define the lines to display.

### frame_add
*Synopsys:*
```C
void frame_add(struct sFrame *sframe, const char *format, ...);
```
*Description:*
This function is used to transfer text, variable values or expression results to all the lines identified by the *sFrame* structure passed as a parameter.
- **sframe**: Pointer to the *sFrame* structure defined by the associated *frame_ini* function.
- **format**: This is the string that contains the text to be written to stdout. It can optionally contain embedded format tags that are replaced by the values specified in subsequent additional arguments and formatted as requested. Refer to printf-like functions.
- **additional arguments**: Depending on the format string, the function may expect a sequence of additional arguments, each containing one value to be inserted instead of each %-tag specified in the format parameter (if any). There should be the same number of these arguments as the number of %-tags that expect a value.

*Return value:*
None.

### frame_close
*Synopsys:*
```C
void frame_close(struct sFrame *sframe, FILE *stream, unsigned short frame);
```
*Description:*
This function displays, in a frame, all the stored lines. The frame type and the appearance (bold or not) are set during this call. The *sFrame* structure allocated by **frame_ini** is freed.
- **sframe**: Pointer to the *sFrame* structure defined by the associated *frame_ini* function.
- **stream**: This is the pointer to a FILE object that identifies the stream (e.g. *stdout*).
- **frame**: This is the result of ORing all constant defining handled forms, appearence and algnment:
  - NOFRAME, TEXTFRAME, SINGLEFRAME, DOUBLEFRAME or BLOCKFRAME,
  - BOLDFRAME (xterm-like only),
  - LEFT, CENTER or RIGHT (xterm-like only).

*Return value:*
None.

### frame_abort
*Synopsys:*
```C
void frame_abort(struct sFrame *);
```
*Description:*
This function aborts the display and frees the *sFrame* structure allocated by **frame_ini**.
*Return value:*
None.

### frame_testWidth
*Synopsys:*
```C
int frame_testWidth(struct sFrame *sframe, unsigned short frame);
```
*Description:*
This function test whether the display window is large enough to display the stored lines and their frame. **[LINUX only]**.
- **sframe**: Pointer to the *sFrame* structure defined by the associated *frame_ini* function.
- **frame**: This is the result of ORing all constant defining handled forms. Refer to **frame_close**.

*Return value:*
- **1** yes
- **0** no

## EXAMPLE
```C
//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
...
//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "frame.h"
//------------------------------------------------------------------------------
// MACROS
//------------------------------------------------------------------------------
#define ARRAY_SIZE(x)	(sizeof(x) / sizeof((x)[0]))
//------------------------------------------------------------------------------
// MAIN
//------------------------------------------------------------------------------
int main(void) {
	const char *victorHugo[7]= {
		"Cosette était laide. Heureuse, elle eut peut-être été jolie. Nous avons déjà esquissé cette petite figure sombre.",
		"Cosette était maigre et blême. Elle avait prés de huit ans, on lui en eut donné à peine six. Ses grands yeux",
		"enfoncés dans une sorte d'ombre profonde étaient presque éteints à force d'avoir pleuré. Les coins de sa bouche",
		"avaient cette courbe de l'angoisse habituelle, qu'on observe chez les condamnés et chez les malades désespérés.",
		"Ses mains étaient, comme sa mère l'avait deviné, « perdues d’engelures ». Le feu qui l'éclairait en ce moment",
		"faisait saillir les angles de ses os et rendait sa maigreur affreusement visible. Comme elle grelotait toujours,",
		"elle avait pris l'habitude de serrer ses deux genoux l'un contre l'autre."
	};
	struct sFrame *fr1, * fr2;
	int k;
	long unsigned int i;
	unsigned short mode = BOLDFRAME | SINGLEFRAME | LEFT;

	fr1 = frame_ini();
	for (i = 0; i < ARRAY_SIZE(victorHugo); i++) frame_add(fr1, "%s\n", victorHugo[i]);
	k = frame_testWidth(fr1, mode);
	if (k) {
		frame_close(fr1, stdout, mode);
	} else {
		fprintf(stdout, "The window is NOT wide enough to display\n\n");
		frame_abort(fr1);
	}
	//---- Exit ----------------------------------------------------------------
	return EXIT_SUCCESS;
}
//------------------------------------------------------------------------------
```

## STRUCTURE OF THE APPLICATION
This section walks you through **libFrame**'s structure. Once you understand this structure, you will easily find your way around in **libFrame**'s code base.

``` bash
$ yaTree
./                        # Application level
├── README_images/        # Images for documentation
│   ├── 1.png             # Example no frame
│   ├── 2.png             # Example text frame
│   ├── 3.png             # Example single frame
│   ├── 4.png             # Example double frame
│   └── 5.png             # Example block frame
├── examples/             # Examples directory
│   ├── Makefile          # Makefile
│   └── basic.c           # Example of 'libFrame' usage
├── src/                  # libFrame.a Source directory
│   ├── Makefile          # Makefile
│   ├── frame.h           # libFrame header file
│   ├── frame_abort.c     # Source file of the primitive with the same name
│   ├── frame_add.c       # Source file of the primitive with the same name
│   ├── frame_close.c     # Source file of the primitive with the same name
│   ├── frame_funcs.c     # Source file of the common routine set
│   ├── frame_ini.c       # Source file of the primitive with the same name
│   ├── frame_internal.h  # Internal header file
│   └── frame_testWidth.c # Source file of the primitive with the same name
├── COPYING.md            # GNU General Public License markdown file
├── LICENSE.md            # License markdown file
├── Makefile              # Makefile
├── README.md             # ReadMe markdown file
├── RELEASENOTES.md       # Release Notes markdown file
└── VERSION               # Version identification text file

3 directories, 22 files
$ 
```
## HOW TO BUILD THIS APPLICATION IN DEBUG MODE ON LINUX
```bash
$ cd libFrame
$ make clean all
```

## HOW TO BUILD THIS APPLICATION IN RELEASE MODE ON LINUX
```bash
$ cd libFrame
$ make release
    # Library generated with -O2 option and the header file, are respectively installed in $LIB_DIR and $INC_DIR directories (defined at environment level).
```

## HOW TO BUILD THIS APPLICATION IN DEBUG MODE ON LINUX FOR WINDOWS
```bash
$ cd libFrame
$ make wclean wall
```

## HOW TO CHECK THIS PACKAGE
```bash
$ cd libFrame
$ make clean all
$ ./linux/testFrame
```
or
```bash
$ cd libFrame
$ make wclean wall
$ ... transfer ./windows/testFrame.exe to a Windows platform and execute it ...
```
## SOFTWARE REQUIREMENTS
- For development:
    - *mingw (C compiler)* packages to build for Windows platform.
- For usage, nothing particular...
- Developped and tested on XUBUNTU 19.04, GCC v8.3.0

## RELEASE NOTES
Refer to file [RELEASENOTES](./RELEASENOTES.md).

***