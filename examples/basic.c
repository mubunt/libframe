//------------------------------------------------------------------------------
// Copyright (c) 2019, Michel Rizzo.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: libFrame
// Displays, in a frame, a set of lines after buffering them
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>
#include <signal.h>
#include <stdbool.h>
#include <ctype.h>
#include <signal.h>
//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "frame.h"
//------------------------------------------------------------------------------
// MACROS
//------------------------------------------------------------------------------
#define ARRAY_SIZE(x)	(sizeof(x) / sizeof((x)[0]))

#define XBOLD			"\033[1m"
#define XRESET			"\033[0m"
#define XBLUE			"\033[37;44m"
//------------------------------------------------------------------------------
// ROUTINES
//------------------------------------------------------------------------------
void test(unsigned short ftype, const char *testnumber) {
#ifdef LINUX
	const char *victorHugo[7]= {
		"Cosette était laide. Heureuse, elle eut peut-être été jolie. Nous avons déjà esquissé cette petite figure sombre.",
		"Cosette était maigre et blême. Elle avait prés de huit ans, on lui en eut donné à peine six. Ses grands yeux",
		"enfoncés dans une sorte d'ombre profonde étaient presque éteints à force d'avoir pleuré. Les coins de sa bouche",
		"avaient cette courbe de l'angoisse habituelle, qu'on observe chez les condamnés et chez les malades désespérés.",
		"Ses mains étaient, comme sa mère l'avait deviné, « perdues d’engelures ». Le feu qui l'éclairait en ce moment",
		"faisait saillir les angles de ses os et rendait sa maigreur affreusement visible. Comme elle grelotait toujours,",
		"elle avait pris l'habitude de serrer ses deux genoux l'un contre l'autre."
	};
	const char *sanAntonio[5] = {
		"Effectivement, en ce matin dominical, lorsque je me présente devant le domicile des Bérurier, leur trottoir",
		"ressemble à la Norvège. Car le sapin du Gros mesure dans les cinq mètres et il disparait derrière.",
		"Leur ami le coiffeur est venu faire ses adieux émus. On se congratule, on charge le roi de la forêt sur la",
		"galerie de ma chignole, la mère Béru, rutilante dans une robe de satin rose bonbon monte a l'arrière, prés de",
		"Félicie et s'assied sur les petits-fours que ma brave femme de mère se faisait une joie d'emporter."
	};
#else
	const char *victorHugo[7]= {
		"Cosette etait laide. Heureuse, elle eut peut-etre ete jolie. Nous avons dejà esquisse cette petite figure sombre.",
		"Cosette etait maigre et bleme. Elle avait pres de huit ans, on lui en eut donne à peine six. Ses grands yeux",
		"enfonces dans une sorte d'ombre profonde etaient presque eteints à force d'avoir pleure. Les coins de sa bouche",
		"avaient cette courbe de l'angoisse habituelle, qu'on observe chez les condamnes et chez les malades desesperes.",
		"Ses mains etaient, comme sa mère l'avait deviné, 'perdues d’engelures'. Le feu qui l'éclairait en ce moment",
		"faisait saillir les angles de ses os et rendait sa maigreur affreusement visible. Comme elle grelotait toujours,",
		"elle avait pris l'habitude de serrer ses deux genoux l'un contre l'autre."
	};
	const char *sanAntonio[5] = {
		"Effectivement, en ce matin dominical, lorsque je me présente devant le domicile des Bérurier, leur trottoir",
		"ressemble à la Norvège. Car le sapin du Gros mesure dans les cinq mètres et il disparait derrière.",
		"Leur ami le coiffeur est venu faire ses adieux émus. On se congratule, on charge le roi de la forêt sur la",
		"galerie de ma chignole, la mère Béru, rutilante dans une robe de satin rose bonbon monte a l'arrière, prés de",
		"Félicie et s'assied sur les petits-fours que ma brave femme de mère se faisait une joie d'emporter."
	};
#endif
	struct sFrame *fr1, * fr2;
	int k;
	long unsigned int i;

	fprintf(stdout, "TEST #%s\n", testnumber);

	fr1 = frame_ini();
	fr2 = frame_ini();
	for (i = 0; i < ARRAY_SIZE(victorHugo); i++) frame_add(fr1, "%s\n", victorHugo[i]);
	for (i = 0; i < ARRAY_SIZE(sanAntonio); i++) frame_add(fr2, "%s\n", sanAntonio[i]);
	k = frame_testWidth(fr1, ftype);
	if (k) {
		frame_close(fr1, stdout, ftype);
		frame_close(fr2, stdout, ftype);
	} else {
		fprintf(stdout, "The window is NOT wide enough to display\n\n");
		frame_abort(fr1);
		frame_abort(fr2);
	}
}

void testwithesc(unsigned short ftype, const char *testnumber) {
#ifdef LINUX
	const char *cliveCussler[8] = {
		"- Vous êtes en retard.",
		"- Désolé, mais ce genre d'opérations prend du temps. Ou en êtes-vous pour le carburant ?",
		"- En train de racler les fonds de tiroirs, repond Steiger. Je peux tenir encore dix-huit ou",
		"  vingt minutes avec de la chance.",
		"- Un navire de croisière norvégien a mis en panne à une centaine de kilomètres au cap 2-7-0.",
		"  Son capitaine a fait évacuer le pont-promenade pour vous recevoir. Vous y arriverez les doigts dans le nez.",
		"- Vous ne seriez pas un peu givré, par hasard ? coupe Steiger. Un navire de croisière norvégien,",
		"  un pont-promenade... et puis quoi encore ?"
	};
#else
	const char *cliveCussler[8] = {
		"- Vous etes en retard.",
		"- Desole, mais ce genre d'operations prend du temps. Ou en etes-vous pour le carburant ?",
		"- En train de racler les fonds de tiroirs, repond Steiger. Je peux tenir encore dix-huit ou",
		"  vingt minutes avec de la chance.",
		"- Un navire de croisiere norvegien a mis en panne a une centaine de kilometres au cap 2-7-0.",
		"  Son capitaine a fait evacuer le pont-promenade pour vous recevoir. Vous y arriverez les doigts dans le nez.",
		"- Vous ne seriez pas un peu givre, par hasard ? coupe Steiger. Un navire de croisiere norvegien,",
		"  un pont-promenade... et puis quoi encore ?"
	};
#endif
	struct sFrame *fr1, *fr2;
	long unsigned int i;

	fprintf(stdout, "TEST #%s (xterm-like only)\n", testnumber);

	fr1 = frame_ini();
	fr2 = frame_ini();
	for (i = 0; i < ARRAY_SIZE(cliveCussler); i++) frame_add(fr1, "%s%s%s\n", XBOLD, cliveCussler[i], XRESET);
	for (i = 0; i < ARRAY_SIZE(cliveCussler); i++) frame_add(fr2, "%s%s%s%s\n", XBLUE, XBOLD, cliveCussler[i], XRESET);
	frame_close(fr1, stdout, ftype);
	frame_close(fr2, stdout, ftype);
}

void additional_test(unsigned short ftype, const char *testnumber) {
	struct sFrame *fr1;

	fprintf(stdout, "TEST #%s (xterm-like only)\n", testnumber);

	fr1 = frame_ini();
	frame_add(fr1, "%s             ________________________________________________   %s\n", XBOLD, XRESET);
	frame_add(fr1, "%s            /                                                \\%s\n", XBOLD, XRESET);
	frame_add(fr1, "%s           |    _________________________________________     |%s\n", XBOLD, XRESET);
	frame_add(fr1, "%s           |   |                                         |    |%s\n", XBOLD, XRESET);
	frame_add(fr1, "%s           |   |                                         |    |%s\n", XBOLD, XRESET);
	frame_add(fr1, "%s           |   |  CHARTREUSE Toolset 2022.4              |    |%s\n", XBOLD, XRESET);
	frame_add(fr1, "%s           |   |  Linux-based command-line environment   |    |%s\n", XBOLD, XRESET);
	frame_add(fr1, "%s           |   |                                         |    |%s\n", XBOLD, XRESET);
	frame_add(fr1, "%s           |   |                                         |    |%s\n", XBOLD, XRESET);
	frame_add(fr1, "%s           |   |                                         |    |%s\n", XBOLD, XRESET);
	frame_add(fr1, "%s           |   |                                         |    |%s\n", XBOLD, XRESET);
	frame_add(fr1, "%s           |   |                                         |    |%s\n", XBOLD, XRESET);
	frame_add(fr1, "%s           |   |                                         |    |%s\n", XBOLD, XRESET);
	frame_add(fr1, "%s           |   |                                         |    |%s\n", XBOLD, XRESET);
	frame_add(fr1, "%s           |   |                                         |    |%s\n", XBOLD, XRESET);
	frame_add(fr1, "%s           |   |                                         |    |%s\n", XBOLD, XRESET);
	frame_add(fr1, "%s           |   |_________________________________________|    |%s\n", XBOLD, XRESET);
	frame_add(fr1, "%s           |                                                  |%s\n", XBOLD, XRESET);
	frame_add(fr1, "%s            \\_________________________________________________/%s\n", XBOLD, XRESET);
	frame_add(fr1, "%s                   \\___________________________________/%s\n", XBOLD, XRESET);
	frame_add(fr1, "%s                ___________________________________________%s\n", XBOLD, XRESET);
	frame_add(fr1, "%s             _-'    .-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.  --- `-_%s\n", XBOLD, XRESET);
	frame_add(fr1, "%s          _-'.-.-. .---.-.-.-.-.-.-.-.-.-.-.-.-.-.-.--.  .-.-.`-_%s\n", XBOLD, XRESET);
	frame_add(fr1, "%s       _-'.-.-.-. .---.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-`__`. .-.-.-.`-_%s\n", XBOLD, XRESET);
	frame_add(fr1, "%s    _-'.-.-.-.-. .-----.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-----. .-.-.-.-.`-_%s\n", XBOLD, XRESET);
	frame_add(fr1, "%s _-'.-.-.-.-.-. .---.-. .-------------------------. .-.---. .---.-.-.-.`-_%s\n", XBOLD, XRESET);
	frame_add(fr1, "%s:-------------------------------------------------------------------------:%s\n", XBOLD, XRESET);
	frame_add(fr1, "%s`---._.-------------------------------------------------------------._.---'%s\n", XBOLD, XRESET);
	frame_close(fr1, stdout, ftype);

	fr1 = frame_ini();
	frame_add(fr1, "%s%s             ________________________________________________              %s\n", XBLUE,XBOLD, XRESET);
	frame_add(fr1, "%s%s            /                                                \\             %s\n", XBLUE,XBOLD, XRESET);
	frame_add(fr1, "%s%s           |    _________________________________________     |            %s\n", XBLUE,XBOLD, XRESET);
	frame_add(fr1, "%s%s           |   |                                         |    |            %s\n", XBLUE,XBOLD, XRESET);
	frame_add(fr1, "%s%s           |   |                                         |    |            %s\n", XBLUE,XBOLD, XRESET);
	frame_add(fr1, "%s%s           |   |  CHARTREUSE Toolset 2022.4              |    |            %s\n", XBLUE,XBOLD, XRESET);
	frame_add(fr1, "%s%s           |   |  Linux-based command-line environment   |    |            %s\n", XBLUE,XBOLD, XRESET);
	frame_add(fr1, "%s%s           |   |                                         |    |            %s\n", XBLUE,XBOLD, XRESET);
	frame_add(fr1, "%s%s           |   |                                         |    |            %s\n", XBLUE,XBOLD, XRESET);
	frame_add(fr1, "%s%s           |   |                                         |    |            %s\n", XBLUE,XBOLD, XRESET);
	frame_add(fr1, "%s%s           |   |                                         |    |            %s\n", XBLUE,XBOLD, XRESET);
	frame_add(fr1, "%s%s           |   |                                         |    |            %s\n", XBLUE,XBOLD, XRESET);
	frame_add(fr1, "%s%s           |   |                                         |    |            %s\n", XBLUE,XBOLD, XRESET);
	frame_add(fr1, "%s%s           |   |                                         |    |            %s\n", XBLUE,XBOLD, XRESET);
	frame_add(fr1, "%s%s           |   |                                         |    |            %s\n", XBLUE,XBOLD, XRESET);
	frame_add(fr1, "%s%s           |   |                                         |    |            %s\n", XBLUE,XBOLD, XRESET);
	frame_add(fr1, "%s%s           |   |_________________________________________|    |            %s\n", XBLUE,XBOLD, XRESET);
	frame_add(fr1, "%s%s           |                                                  |            %s\n", XBLUE,XBOLD, XRESET);
	frame_add(fr1, "%s%s            \\_________________________________________________/            %s\n", XBLUE,XBOLD, XRESET);
	frame_add(fr1, "%s%s                   \\___________________________________/                   %s\n", XBLUE,XBOLD, XRESET);
	frame_add(fr1, "%s%s                ___________________________________________                %s\n", XBLUE,XBOLD, XRESET);
	frame_add(fr1, "%s%s             _-'    .-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.  --- `-_             %s\n", XBLUE,XBOLD, XRESET);
	frame_add(fr1, "%s%s          _-'.-.-. .---.-.-.-.-.-.-.-.-.-.-.-.-.-.-.--.  .-.-.`-_          %s\n", XBLUE,XBOLD, XRESET);
	frame_add(fr1, "%s%s       _-'.-.-.-. .---.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-`__`. .-.-.-.`-_       %s\n", XBLUE,XBOLD, XRESET);
	frame_add(fr1, "%s%s    _-'.-.-.-.-. .-----.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-----. .-.-.-.-.`-_    %s\n", XBLUE,XBOLD, XRESET);
	frame_add(fr1, "%s%s _-'.-.-.-.-.-. .---.-. .-------------------------. .-.---. .---.-.-.-.`-_ %s\n", XBLUE,XBOLD, XRESET);
	frame_add(fr1, "%s%s:-------------------------------------------------------------------------:%s\n", XBLUE,XBOLD, XRESET);
	frame_add(fr1, "%s%s`---._.-------------------------------------------------------------._.---'%s\n", XBLUE,XBOLD, XRESET);
	frame_close(fr1, stdout, ftype);
}
//------------------------------------------------------------------------------
// MAIN
//------------------------------------------------------------------------------
int main(void) {
	//----  Go on --------------------------------------------------------------
	signal(SIGABRT, SIG_IGN);
	signal(SIGTERM, SIG_IGN);
	signal(SIGINT, SIG_IGN);
	//---- Tests ---------------------------------------------------------------
	test(LEFTFRAME | NOFRAME, "1.1 - NO FRAME, LEFT");
	test(LEFTFRAME | TEXTFRAME, "1.2 - TEXT FRAME, LEFT");
	test(LEFTFRAME | SINGLEFRAME, "1.3 - SINGLE FRAME, LEFT");
	test(LEFTFRAME | DOUBLEFRAME, "1.4 - DOUBLE FRAME, LEFT");
	test(LEFTFRAME | BLOCKFRAME, "1.5 - BLOCK FRAME, LEFT");

	test(LEFTFRAME | BOLDFRAME | NOFRAME, "2.1 - NO FRAME, LEFT, BOLD (xterm-like only)");
	test(LEFTFRAME | BOLDFRAME | TEXTFRAME, "2.2 - TEXT , LEFT, BOLD (xterm-like only)");
	test(LEFTFRAME | BOLDFRAME | SINGLEFRAME, "2.3 - SINGLE FRAME, LEFT, LEFT, BOLD (xterm-like only)");
	test(LEFTFRAME | BOLDFRAME | DOUBLEFRAME, "2.4 - DOUBLE FRAME, LEFT, BOLD (xterm-like only)");
	test(LEFTFRAME | BOLDFRAME | BLOCKFRAME, "2.5 - BLOCK FRAME, LEFT, BOLD (xterm-like only)");

	testwithesc(CENTERFRAME | NOFRAME, "3.1- NO FRAME, CENTER");
	testwithesc(CENTERFRAME | BOLDFRAME | NOFRAME, "3.1bis- NO FRAME, CENTER, BOLD (xterm-like only)");
	testwithesc(CENTERFRAME | TEXTFRAME, "3.2 - TEXT FRAME, CENTER");
	testwithesc(CENTERFRAME | SINGLEFRAME, "3.3 - SINGLE FRAME, CENTER");
	testwithesc(CENTERFRAME | DOUBLEFRAME, "3.4 - DOUBLE FRAME, CENTER");
	testwithesc(CENTERFRAME | BLOCKFRAME, "3.5 - BLOCK FRAME, CENTER");
	testwithesc(CENTERFRAME | DOUBLEFRAME, "3.x (replaces tests 3.1 to 3.5)");

	additional_test(RIGHTFRAME | NOFRAME, "4.1- NO FRAME, RIGHT");
	additional_test(RIGHTFRAME | TEXTFRAME, "4.2 - TEXT FRAME, RIGHT");
	additional_test(RIGHTFRAME | SINGLEFRAME, "4.3 - SINGLE FRAME, RIGHT");
	additional_test(RIGHTFRAME | DOUBLEFRAME, "4.4 - DOUBLE FRAME, RIGHT");
	additional_test(RIGHTFRAME | BLOCKFRAME, "4.5 - BLOCK FRAME, RIGHT");
	additional_test(RIGHTFRAME | DOUBLEFRAME, "4.x (replaces tests 4.1 to 4.5)");
	//---- Exit ----------------------------------------------------------------
	return EXIT_SUCCESS;
}
//------------------------------------------------------------------------------
