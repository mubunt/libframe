# RELEASE NOTES: *libFrame*, Displays, in a frame, a set of lines after buffering them.

Functional limitations, if any, of this version are described in the *README.md* file.

 **Version 1.2.1**:
  - Added the freeing of the  allocated memory for header part of line before redefining it!!!

 **Version 1.2.0**:
  - Fixed bug (initialization of header part of lines; was null instead of addresse of a null string).

- **Version 1.1.10**:
  - Updated build system components.

- **Version 1.1.9**:
  - Updated build system.

- **Version 1.1.8**:
  - Removed unused files.

- **Version 1.1.7**:
  - Updated build system component(s)

- **Version 1.1.6**:
  - Renamed executable built as example as *example_xxx* to be compliant with other projects.

- **Version 1.1.5**:
  - Reworked build system to ease global and inter-project updated.
  - Run *astyle* on sources.
  - Added *cppcheck* target (Static C code analysis) and run it.

- **Version 1.1.4**:
  - Some minor changes in .comment file(s).

- **Version 1.1.3**:
  - Standardization of the installation of executables and libraries in $BIN_DIR, $LIB_DIR anetd $INC_DIR defined in the environment.

- **Version 1.1.2**:
  - Updated makefiles and asociated README information.

- **Version 1.1.1**:
  - Fixed bug in escape color code management.

- **Version 1.1.0**:
  - Added alignment (left, center, right) for xterm-like only.
  - Changed parameter type of *frame_close* and *frame_testWidth* from *short* to *unsigned short*.
  - Hided definition of structure *sFrame* to public.

- **Version 1.0.1**:
  - Fixed ./Makefile and ./src/Makefile.

- **Version 1.0.0**:
  - First version.
  